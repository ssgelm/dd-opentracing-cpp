dd-opentracing-cpp (1.3.7-1) unstable; urgency=medium

  * New upstream release
  * Replace libcurl4-nss-dev dep with libcurl4-openssl-dev (Closes: #1038909)
  * Update standards version to 4.6.2

 -- Stephen Gelman <ssgelm@debian.org>  Sun, 13 Aug 2023 23:48:41 -0500

dd-opentracing-cpp (1.3.6-1) unstable; urgency=medium

  * New upstream release
  * Fix FTBFS on riscv64 (Closes: #1022112)
    Thanks to JunYuan Tan for the patch!

 -- Stephen Gelman <ssgelm@debian.org>  Mon, 24 Oct 2022 10:34:12 -0500

dd-opentracing-cpp (1.3.1-3) unstable; urgency=medium

  * Change dep from libmsgpack-dev to libmsgpack-cxx-dev (Closes: #1019111)
  * Update standards version to 4.6.1.0

 -- Stephen Gelman <ssgelm@debian.org>  Tue, 11 Oct 2022 19:04:39 -0500

dd-opentracing-cpp (1.3.1-2) unstable; urgency=medium

  * Update catch2 dependency to fix FTBFS with glibc >= 2.34 (Closes: #1009662)
    Thanks to Steve Langasek for the patch!
  * Add patch to fix FTBFS in span_test
  * Update standards version to 4.6.0.1

 -- Stephen Gelman <ssgelm@debian.org>  Sun, 24 Apr 2022 19:40:49 -0500

dd-opentracing-cpp (1.3.1-1) unstable; urgency=medium

  * New upstream release

 -- Stephen Gelman <ssgelm@debian.org>  Sun, 07 Nov 2021 00:52:03 -0500

dd-opentracing-cpp (1.3.0-1) experimental; urgency=medium

  * Initial release (Closes: #991671)

 -- Stephen Gelman <ssgelm@debian.org>  Thu, 15 Jul 2021 23:05:10 -0500
